#include "ragnaryk.h"
#include "ui_ragnaryk.h"
#include <QtAwesome/QtAwesome.h>
#include <QHotkey/QHotkey/qhotkey.h>
#include <QWebEngineView>
#include <QWebEngineFullScreenRequest>
#include <QtNetwork>
#include <QtWebEngineWidgets/QWebEngineProfile>
#include <QtWebEngineWidgets/QWebEnginePage>

Ragnaryk::Ragnaryk(QWidget *parent) : QMainWindow(parent), ui(new Ui::Ragnaryk) {
    ui->setupUi(this);

    connect(ui->browser->page(), &QWebEnginePage::fullScreenRequested, this, &Ragnaryk::acceptFullScreenRequest);


    QtAwesome* awesome = new QtAwesome(qApp);
    awesome->initFontAwesome();

    QVariantMap options;
    options.insert( "color" , QColor(255,255,255) );


    ui->browser->settings()->setDefaultTextEncoding("utf-8");
    ui->browser->settings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::Accelerated2dCanvasEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::AllowGeolocationOnInsecureOrigins, false);
    ui->browser->settings()->setAttribute(QWebEngineSettings::AllowRunningInsecureContent, false);
    ui->browser->settings()->setAttribute(QWebEngineSettings::ErrorPageEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::JavascriptCanAccessClipboard, false);
    ui->browser->settings()->setAttribute(QWebEngineSettings::JavascriptEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::LocalStorageEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::ScrollAnimatorEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::SpatialNavigationEnabled, true);
    ui->browser->settings()->setAttribute(QWebEngineSettings::XSSAuditingEnabled, true);
    ui->browser->page()->profile()->setHttpUserAgent(QLatin1String("Ragnaryk/1.0"));

    ui->back_btn->setIcon(awesome->icon(fa::chevronleft, options));
    ui->forward_btn->setIcon(awesome->icon(fa::chevronright, options));
    ui->go_btn->setIcon(awesome->icon(fa::arrowright, options));
    ui->refresh_btn->setIcon(awesome->icon(fa::refresh, options));
    ui->settings_btn->setIcon(awesome->icon(fa::cogs, options));

    ui->browser->load(homepage);
    ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());

    QAction* openInNewTab = ui->browser->pageAction(QWebEnginePage::OpenLinkInNewTab);
    ui->browser->addAction(openInNewTab);
    connect(openInNewTab, SIGNAL(triggered()), this, SLOT(openInNewTab()));

    ui->tabWidget->addTab(new QLabel("+"), QString("+"));
    connect(ui->tabWidget, &QTabWidget::currentChanged, this, &Ragnaryk::on_tabWidget_currentChanged);


}

Ragnaryk::~Ragnaryk() {
  delete ui;
}

void Ragnaryk::on_go_btn_clicked() {
    QString url = ui->url_edit->text();

    QNetworkAccessManager nam;
    QNetworkRequest req((QUrl(url)));
    req.setRawHeader("User-Agent", "NeoRagnaryk/1.0");



    QNetworkReply *reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if(reply->bytesAvailable()) {
        // url accesible
        ui->browser->load(QUrl(url));
        //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
    }
    else if(url.contains("http://") || url.contains("https://")) {
        QNetworkAccessManager nam;
        QNetworkRequest req((QUrl(url)));
        QNetworkReply *reply = nam.get(req);
        QEventLoop loop;

        connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();
        if(reply->bytesAvailable()) {
            // url accesible
            ui->browser->load(QUrl(url));
            //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
        } else {
            url.remove(0, 7);
            url = "https://duckduckgo.com/?q=" + url;
            ui->browser->load(QUrl(url));
            //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
        }
    } else {
        url.insert(0,"http://");
        ui->url_edit->setText(url);
        QNetworkAccessManager nam;
        QNetworkRequest req((QUrl(url)));
        QNetworkReply *reply = nam.get(req);
        QEventLoop loop;
        connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();
        if(reply->bytesAvailable()) {
            // url accesible
            ui->browser->load(QUrl(url));
            //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
        } else {
            url.remove(0, 7);
            url = "https://duckduckgo.com/?q=" + url;
            ui->browser->load(QUrl(url));
            //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
        }
    }

}

void Ragnaryk::on_back_btn_clicked() {
    ui->browser->back();
    //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
}

void Ragnaryk::on_forward_btn_clicked() {
    ui->browser->forward();
    //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
}

void Ragnaryk::on_refresh_btn_clicked() {
    ui->browser->reload();
    //ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
}

void Ragnaryk::on_browser_loadProgress(int progress) {
    ui->progressBar->setValue(progress);
}

void Ragnaryk::on_actionAbout_Ragnaryk_triggered() {
  QMessageBox AboutBox;
  AboutBox.setText("Ragnaryk is free software (GPLv3) and was brought to you by Pascal Peinecke (<a href='https://www.gitlab.com/Pascal3366'>https://www.gitlab.com/Pascal3366</a>)");
  AboutBox.exec();
}

void Ragnaryk::on_browser_urlChanged(const QUrl &url) {
  ui->url_edit->setText(QString(url.toEncoded()));
  ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->browser->page()->title());
  //ui->tabWidget->addTab(ui->browser, ui->browser->page()->title());
}

void Ragnaryk::on_url_edit_returnPressed() {
  emit Ragnaryk::on_go_btn_clicked();
}

void Ragnaryk::refresh() {
  ui->browser->reload();
}

void Ragnaryk::on_browser_loadFinished(bool finished) {
    if(finished) {
        ui->progressBar->hide();
    }
}

void Ragnaryk::on_browser_loadStarted() {
    ui->progressBar->show();
}

void Ragnaryk::acceptFullScreenRequest(QWebEngineFullScreenRequest request) {
        request.accept();
        const auto windowGeometry = qApp->desktop()->availableGeometry(0);
        setCentralWidget(ui->browser);
        ui->browser->move(windowGeometry.topLeft());
        ui->browser->resize(windowGeometry.size());

}

void Ragnaryk::fullScreenRequested(QWebEngineFullScreenRequest request) {
  request.accept();
  const auto windowGeometry = qApp->desktop()->availableGeometry(0);
  setCentralWidget(ui->browser);
  ui->browser->move(windowGeometry.topLeft());
  ui->browser->resize(windowGeometry.size());

}

void Ragnaryk::openInNewTab() {
  qDebug() << "newTab trigered";
  //ui->browser->page()->
  //ui->tabWidget->addTab(ui->browser, ui->browser->page()->title());
  //ui->tabWidget->insertTab(ui->tabWidget->count(), ui->tabWidget, QIcon("icons/example_favicon.ico"), "newTab");
  //ui->tabWidget->addTab(new QWidget(), "new Tab");
  //ui->tabWidget->1

  //QWebEngineView* browser = new QWebEngineView();
  ui->tabWidget->addTab(ui->browser,"Tab2");
  //ui->tabWidget->insertTab(ui->tabWidget->currentIndex() + 1, ui->browser, "newTab");

  //ui->tabWidget->setCurrentWidget(ui->browser);
  //ui->tabWidget->count() - ui->tabWidget->currentIndex() + 1;
}


void Ragnaryk::on_tabWidget_currentChanged(int index) {
  if (index == ui->tabWidget->count() - 1) {
      newTab();
  }
}

void Ragnaryk::newTab() {
    int position = ui->tabWidget->count() - 1;
    ui->tabWidget->insertTab(position, new QLabel("Your new tab here"), QString("New tab"));
    ui->tabWidget->setCurrentIndex(position);
    auto tabBar = ui->tabWidget->tabBar();
    tabBar->scroll(tabBar->width(), 0);
}
