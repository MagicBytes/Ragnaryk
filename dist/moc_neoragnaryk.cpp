/****************************************************************************
** Meta object code from reading C++ file 'neoragnaryk.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/neoragnaryk.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'neoragnaryk.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NeoRagnaryk_t {
    QByteArrayData data[18];
    char stringdata0[347];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NeoRagnaryk_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NeoRagnaryk_t qt_meta_stringdata_NeoRagnaryk = {
    {
QT_MOC_LITERAL(0, 0, 11), // "NeoRagnaryk"
QT_MOC_LITERAL(1, 12, 17), // "on_go_btn_clicked"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 19), // "on_back_btn_clicked"
QT_MOC_LITERAL(4, 51, 22), // "on_forward_btn_clicked"
QT_MOC_LITERAL(5, 74, 22), // "on_refresh_btn_clicked"
QT_MOC_LITERAL(6, 97, 23), // "on_browser_loadProgress"
QT_MOC_LITERAL(7, 121, 8), // "progress"
QT_MOC_LITERAL(8, 130, 36), // "on_actionAbout_NeoRagnaryk_tr..."
QT_MOC_LITERAL(9, 167, 21), // "on_browser_urlChanged"
QT_MOC_LITERAL(10, 189, 4), // "arg1"
QT_MOC_LITERAL(11, 194, 25), // "on_url_edit_returnPressed"
QT_MOC_LITERAL(12, 220, 23), // "on_browser_loadFinished"
QT_MOC_LITERAL(13, 244, 22), // "on_browser_loadStarted"
QT_MOC_LITERAL(14, 267, 23), // "acceptFullScreenRequest"
QT_MOC_LITERAL(15, 291, 27), // "QWebEngineFullScreenRequest"
QT_MOC_LITERAL(16, 319, 7), // "request"
QT_MOC_LITERAL(17, 327, 19) // "fullScreenRequested"

    },
    "NeoRagnaryk\0on_go_btn_clicked\0\0"
    "on_back_btn_clicked\0on_forward_btn_clicked\0"
    "on_refresh_btn_clicked\0on_browser_loadProgress\0"
    "progress\0on_actionAbout_NeoRagnaryk_triggered\0"
    "on_browser_urlChanged\0arg1\0"
    "on_url_edit_returnPressed\0"
    "on_browser_loadFinished\0on_browser_loadStarted\0"
    "acceptFullScreenRequest\0"
    "QWebEngineFullScreenRequest\0request\0"
    "fullScreenRequested"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NeoRagnaryk[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x08 /* Private */,
       3,    0,   75,    2, 0x08 /* Private */,
       4,    0,   76,    2, 0x08 /* Private */,
       5,    0,   77,    2, 0x08 /* Private */,
       6,    1,   78,    2, 0x08 /* Private */,
       8,    0,   81,    2, 0x08 /* Private */,
       9,    1,   82,    2, 0x08 /* Private */,
      11,    0,   85,    2, 0x08 /* Private */,
      12,    1,   86,    2, 0x08 /* Private */,
      13,    0,   89,    2, 0x08 /* Private */,
      14,    1,   90,    2, 0x08 /* Private */,
      17,    1,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,

       0        // eod
};

void NeoRagnaryk::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<NeoRagnaryk *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_go_btn_clicked(); break;
        case 1: _t->on_back_btn_clicked(); break;
        case 2: _t->on_forward_btn_clicked(); break;
        case 3: _t->on_refresh_btn_clicked(); break;
        case 4: _t->on_browser_loadProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_actionAbout_NeoRagnaryk_triggered(); break;
        case 6: _t->on_browser_urlChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 7: _t->on_url_edit_returnPressed(); break;
        case 8: _t->on_browser_loadFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_browser_loadStarted(); break;
        case 10: _t->acceptFullScreenRequest((*reinterpret_cast< QWebEngineFullScreenRequest(*)>(_a[1]))); break;
        case 11: _t->fullScreenRequested((*reinterpret_cast< QWebEngineFullScreenRequest(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject NeoRagnaryk::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_NeoRagnaryk.data,
    qt_meta_data_NeoRagnaryk,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *NeoRagnaryk::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NeoRagnaryk::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NeoRagnaryk.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int NeoRagnaryk::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
